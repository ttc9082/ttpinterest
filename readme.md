> ** Final Project for CS6083 Fall1134.CS6083.1082: PRNCPLS DATABASE SYSTEMS **
##   TTpinterest

TTpinterest is a [Pinterest](http://www.pinterest.com) like website designed as the final project of the database course.
Hosting at EC2, Storage at S3, Database at RDS. Everything on the cloud [AWS](http://aws.amazon.com).
Tags : Python Django1.6 Bootstrap Boto
Check out the demo below to see details.

## Demo
[Demo here](http://ec2-54-205-86-181.compute-1.amazonaws.com:8080/)

## Main features

- A website people can register and pin pictures
- Likes and Repins and comments
- Creating boards that can be followed by others
- Support pictures uploaded from an url or local file, and they will be stored in [AWS S3](http://aws.amazon.com)
- Friend request and accept
- Privacy polices that can be used to avoid unfriend comments.
- and more.

That's it.
