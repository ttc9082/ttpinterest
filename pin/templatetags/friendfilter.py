from django import template
# Create your views here.
# This is a filter, when loaded, using functions below in templates are enabled.

register = template.Library()

@register.filter
def is_friend(friend, user_id):
    return friend.filter(frded_user=user_id)

@register.filter
def friend_by(friend, user_id):
    return friend.filter(frding_user=user_id)