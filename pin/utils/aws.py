__author__ = 'TTc9082'

from boto.s3.connection import S3Connection
from boto.s3.key import Key
from config import *
from random import *
from time import time
import urllib2
import StringIO

def save_to_s3(img):
    conn = S3Connection(MY_ACCESS_KEY, MY_SECRETE_KEY)
    bucket = conn.get_bucket('tzpin')
    k = Key(bucket)
    ext = img.name[-4:]
    k.key = str(int(time()))+str(randint(0, 9999))+ext
    size = k.set_contents_from_file(img)
    k.make_public()
    url = 'https://s3.amazonaws.com/' + bucket.name + '/' + k.key
    return url

def save_to_s3_url(img):
    conn = S3Connection(MY_ACCESS_KEY, MY_SECRETE_KEY)
    bucket = conn.get_bucket('tzpin')
    k = Key(bucket)
    ext = img[-4:]
    k.key = str(int(time()))+str(randint(0, 9999)) + ext
    fp = StringIO.StringIO(urllib2.urlopen(img).read())
    size = k.set_contents_from_file(fp)
    k.make_public()
    url = 'https://s3.amazonaws.com/' + bucket.name + '/' + k.key
    return url