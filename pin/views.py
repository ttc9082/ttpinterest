from django.shortcuts import render_to_response, render
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.views.decorators.csrf import csrf_protect
from django.core.context_processors import csrf
from .models import *
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from utils import aws
from PIL import Image
import urllib2
# Create your views here.


@csrf_protect
def home(request):
    context = {}
    pin_comments = {}
    pin_like = {}
    all_pins = pin.objects.all()
    for each_pin in all_pins:
        pin_comments[each_pin.id] = []
        comments = each_pin.comment_set.all().order_by('-create_time')
        for each_comment in comments:
            pin_comments[each_pin.id].append(each_comment)
    for each_pin in all_pins:
        likes = each_pin.pic.like_set.count()
        pin_like[each_pin.id] = likes
    context['likes'] = pin_like
    context['pins'] = all_pins
    context['pin_comments'] = pin_comments
    if request.user.is_authenticated():
        context['user'] = request.user
    # context['debug'] = [request.user.pk, pin_like, pin_comments]
    return render(request, 'index.html', context)

def login(request):
    login_form = AuthenticationForm()
    context = {'login_form': login_form}
    context.update(csrf(request))
    if request.method == 'POST':
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return HttpResponseRedirect('/')
        else:
            context['error'] = 'Sorry, Invalid input, Try again.'
            return render_to_response('login.html', context)
    return render_to_response('login.html', context)

def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/')

def reg(request):
    context = {}
    reg_form = UserCreationForm()
    context['reg_form'] = reg_form
    context.update(csrf(request))
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
        else:
            context['error'] = 'input invalid.'
            return render_to_response('reg.html', context)
    return render_to_response('reg.html', context)

@csrf_protect
def create_board(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            name = request.POST.get('board_name')
            new_board = board(user=request.user,
                              create_time=timezone.now(),
                              name=name)
            new_board.save()
    return HttpResponseRedirect('/')

@csrf_protect
@login_required(login_url='/pin/account/login')
def user_profile(request, user_id=1):
    p_user = User.objects.get(pk=user_id)
    p_boards = p_user.board_set.all().order_by('-create_time')
    p_detail = profile.objects.get_or_create(user=p_user)
    context = {'profile_user': p_user,
                       'user': request.user,
                   'p_boards': p_boards,
                      'detail': p_detail[0],
                      'debug': [p_detail[0].avatar.url]
                      }
    if len(request.user.ing.filter(frded_user__id=user_id)):
        if len(User.objects.get(pk=user_id).ing.filter(frded_user__id=request.user.id)):
            context['frd'] = '1'
        elif len(User.objects.get(pk=user_id).ing.filter(frded_user__id=request.user.id)) == 0:
            context['frd'] = '2'
    else:
        context['frd'] = '0'
    context.update(csrf(request))
    return render(request, 'profile.html', context)

@csrf_protect
def create_pin2(request):
    user = request.user
    p_board = user.board_set.all().order_by('-create_time')
    if request.method == 'POST':
        pin_name = request.POST.get('name')
        to_board = board.objects.get(pk=request.POST.get('board_id'))
        picture = request.FILES['picture']
        if picture.name[-3:] in ['jpg', 'png', 'gif', 'PNG', 'JPG', 'GIF']:
            url = aws.save_to_s3(picture)
            pic_file = urllib2.urlopen(url)
            localFile = open('temp.png', 'w')
            localFile.write(pic_file.read())
            localFile.close()
            localFile = open('temp.png', 'r')
            s = Image.open(localFile)
            width, height = s.size
            final_height = height * 190 / width
            new_pic = pic(url=url, create_time=timezone.now(), final_height=final_height)
            new_pic.save()
            new_pin = pin(name=pin_name,
                          pic=new_pic,
                          board=to_board,
                          create_time=timezone.now())
            new_pin.save()
            p_tags = request.POST['tags']
            tag_list = p_tags.split(',')
            for t_tag in tag_list:
                tt = tag.objects.get_or_create(content=t_tag)[0]
                tt.pic.add(new_pic)
            context = {'user': request.user,
                       'message': 'create pin succeed.',
                       'pin': new_pin,
                       'pic': new_pic,
                       'debug': [picture.name, width, height, final_height]}
            return render_to_response('pinned.html', context)
    context = {'user': request.user,
               'boards': p_board,
               'debug':[]}
    context.update(csrf(request))
    return render_to_response('pin_up.html', context)

@login_required(login_url='/pin/account/login')
def comment_pin(request):
    if request.POST['comment']:
        com = comment(user=request.user,
                      pin=pin.objects.get(pk=request.POST.get('pin_id')),
                      content=request.POST.get('comment'),
                      create_time=timezone.now())
        com.save()
    # return render_to_response('index.html', context)
    return HttpResponseRedirect('/')

@login_required(login_url='/pin/account/login')
def like_pin(request, pin_id):
    if pin_id:
        if request.user != pin.objects.get(pk=pin_id).board.user:
            pic = pin.objects.get(pk=pin_id).pic
            like1 = like(user=request.user,
                         pic=pic,
                         create_time=timezone.now())
            like1.save()
    return HttpResponseRedirect('/')

@login_required(login_url='/pin/account/login')
def repin_pin(request, pin_id=1):
    if pin_id:
        if request.method == 'POST':
            origin_pin = pin.objects.get(pk=pin_id)
            new_pin = pin(pic=origin_pin.pic,
                          board=board.objects.get(pk=request.POST['board_id']),
                          create_time=timezone.now(),
                          name=request.POST.get('name')
                          )
            new_pin.save()
            new_repin = repin(from_pin=origin_pin,
                              repin=new_pin,
                              create_time=timezone.now())
            new_repin.save()
            context = {'message': 'Congratulations! Repin succeed.'}
            return render_to_response('repin_succeed.html', context)
        all_boards = request.user.board_set.all()
        context = {'boards': all_boards,
                   'pin_id': pin_id,
                   'user': request.user}
        context.update(csrf(request))
        return render_to_response('repin_page.html',context)

@login_required(login_url='/pin/account/login')
def show_board(request, board_id):
    theBoard = board.objects.get(pk=board_id)
    user_streams = request.user.stream_set.all()
    followed = len(user_streams.filter(board=theBoard))
    context = {}
    pin_comments = {}
    pin_like = {}
    the_pins = theBoard.pin_set.all()
    for each_pin in the_pins:
        pin_comments[each_pin.id] = []
        comments = each_pin.comment_set.all().order_by('-create_time')
        for each_comment in comments:
            pin_comments[each_pin.id].append(each_comment)
    for each_pin in the_pins:
        likes = each_pin.pic.like_set.count()
        pin_like[each_pin.id] = likes
    context['likes'] = pin_like
    context['pins'] = the_pins
    context['pin_comments'] = pin_comments
    if request.user.is_authenticated():
        context['user'] = request.user
    context['theboard'] = theBoard
    context['followed'] = followed
    return render(request, 'board.html', context)

def follow(request, board_id):
    if board_id:
        if request.method == "POST":
            theBoard = board.objects.get(pk=board_id)
            user_streams = request.user.stream_set.all()
            followed = len(user_streams.filter(board=theBoard))
            if followed == 0:
                name = str(request.user.id) + '_' + str(board_id)
                new_stream = stream(user=request.user,
                                    name=name,
                                    create_time=timezone.now())
                new_stream.save()
                new_stream.board.add(theBoard)
            else:
                streams2clean = user_streams.filter(board=theBoard)
                for stream_clean in streams2clean:
                    stream_clean.board.remove(theBoard)
                    if len(stream_clean.board.all()) == 0:
                        stream_clean.delete()
    return HttpResponseRedirect(request.POST['back_url'])

@login_required(login_url='/pin/account/login')
def friend(request, user_id):
    frd = friendship(frding_user=request.user,
               frded_user=User.objects.get(pk=user_id),
               create_time=timezone.now())
    frd.save()
    return HttpResponseRedirect(request.POST['back_url'])

@login_required(login_url='/pin/account/login')
def unfriend(request, user_id):
    frd2 = friendship.objects.filter(frding_user=User.objects.get(pk=user_id), frded_user=request.user)
    frd2.delete()
    try:
        frd = friendship.objects.filter(frding_user=request.user, frded_user=User.objects.get(pk=user_id))
        frd.delete()

    except:
        pass

    return HttpResponseRedirect(request.POST['back_url'])

@login_required(login_url='/pin/account/login')
def change_profile(request):
    if request.method == 'POST':
        user = request.user
        p = user.profile
        if request.FILES:
            picture = request.FILES['avatar']
            if picture.name[-3:] in ['jpg', 'png', 'gif', 'PNG']:
                url = aws.save_to_s3(picture)
                pic_file = urllib2.urlopen(url)
                localFile = open('temp.png', 'w')
                localFile.write(pic_file.read())
                localFile.close()
                localFile = open('temp.png', 'r')
                s = Image.open(localFile)
                width, height = s.size
                final_height = height * 190 / width
                new_pic = pic(url=url, create_time=timezone.now(), final_height=final_height)
                new_pic.save()
                p.avatar = new_pic
        p.unfrd_comment = True if request.POST['uc'] == 'True' else False
        p.save()
    return HttpResponseRedirect(request.POST['back_url'])

@login_required(login_url='/pin/account/login')
def delete_pin(request, pin_id):
    pin1 = pin.objects.get(pk=pin_id)
    pin1.delete()
    return HttpResponseRedirect('/')

@csrf_protect
def create_pin(request):
    user = request.user
    p_board = user.board_set.all().order_by('-create_time')
    if request.method == 'POST':
        pin_name = request.POST.get('name')
        to_board = board.objects.get(pk=request.POST.get('board_id'))
        url = request.POST['picture']
        url_s3 = aws.save_to_s3_url(url)
        pic_file = urllib2.urlopen(url)
        localFile = open('temp.png', 'w')
        localFile.write(pic_file.read())
        localFile.close()
        localFile = open('temp.png', 'r')
        s = Image.open(localFile)
        width, height = s.size
        final_height = height * 190 / width
        new_pic = pic(url=url_s3, create_time=timezone.now(), final_height=final_height)
        new_pic.save()
        new_pin = pin(name=pin_name,
                      pic=new_pic,
                      board=to_board,
                      create_time=timezone.now())
        new_pin.save()
        p_tags = request.POST['tags']
        tag_list = p_tags.split(',')
        for t_tag in tag_list:
            tt = tag.objects.get_or_create(content=t_tag)[0]
            tt.pic.add(new_pic)
        context = {'user': request.user,
                       'message': 'create pin succeed.',
                       'pin': new_pin,
                       'pic': new_pic,
                       'debug': [width, height, final_height]}
        return render_to_response('pinned.html', context)
    context = {'user': request.user,
               'boards': p_board,
               'debug':[]}
    context.update(csrf(request))
    return render_to_response('pin.html', context)

def search_pin(request):
    context = {}
    pin_comments = {}
    pin_like = {}
    key_words = request.GET['key_words']
    keys = key_words.split(' ')
    all_pins = []
    for key in keys:
        t = tag.objects.get(content=key)
        ps = t.pic.distinct()
        for p in ps:
            pinp = p.pin_set.all()[0]
            all_pins.append(pinp)
    for each_pin in all_pins:
        pin_comments[each_pin.id] = []
        comments = each_pin.comment_set.all().order_by('-create_time')
        for each_comment in comments:
            pin_comments[each_pin.id].append(each_comment)
    for each_pin in all_pins:
        likes = each_pin.pic.like_set.count()
        pin_like[each_pin.id] = likes
    context['likes'] = pin_like
    context['pins'] = all_pins
    context['pin_comments'] = pin_comments
    if request.user.is_authenticated():
        context['user'] = request.user
    context['debug'] = [request.user.pk, pin_like, pin_comments]
    return render(request, 'index.html', context)