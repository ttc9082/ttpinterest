from django.db import models
from django.contrib.auth.models import User

class pic(models.Model):
    url = models.TextField()
    create_time = models.DateTimeField()
    final_height = models.IntegerField()
    def __unicode__(self):
        return self.url

class profile(models.Model):
    user = models.OneToOneField(User, primary_key=True)
    avatar = models.ForeignKey(pic, default=1)
    unfrd_comment = models.BooleanField(default=False)
    def __unicode__(self):
        return self.avatar

class board(models.Model):
    name = models.CharField(max_length=100)
    user = models.ForeignKey(User)
    create_time = models.DateTimeField()
    def __unicode__(self):
        return self.name

class pin(models.Model):
    name = models.CharField(max_length=30)
    board = models.ForeignKey(board)
    pic = models.ForeignKey(pic)
    create_time = models.DateTimeField()
    def __unicode__(self):
        return self.name

class friendship(models.Model):
    frding_user = models.ForeignKey(User, related_name='ing')
    frded_user = models.ForeignKey(User, related_name='ed')
    create_time = models.DateTimeField()
    def __unicode__(self):
        return self.frding_user.id

class like(models.Model):
    user = models.ForeignKey(User)
    pic = models.ForeignKey(pic)
    create_time = models.DateTimeField()
    def __unicode__(self):
        return [self.user, self.pic]

class comment(models.Model):
    user = models.ForeignKey(User)
    pin = models.ForeignKey(pin)
    content = models.TextField()
    create_time = models.DateTimeField()
    def __unicode__(self):
        return self.content

class stream(models.Model):
    user = models.ForeignKey(User)
    name = models.CharField(max_length=100)
    board = models.ManyToManyField(board)
    create_time = models.DateTimeField()
    def __unicode__(self):
        return self.name

class repin(models.Model):
    repin = models.ForeignKey(pin, related_name='new')
    from_pin = models.ForeignKey(pin, related_name='from')
    create_time = models.DateTimeField()
    def __unicode__(self):
        return [self.repin, self.from_pin]


class tag(models.Model):
    content = models.CharField(max_length=30)
    pic = models.ManyToManyField(pic)
    def __unicode__(self):
        return self.content
