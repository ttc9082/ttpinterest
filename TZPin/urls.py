from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'TZPin.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'pin.views.home', name='home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^pin/', include('pin.urls')),
)
